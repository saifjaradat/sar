package com.example.sar.home.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.example.sar.base.BaseFragment
import com.example.sar.home.viewmodel.HomeViewModel
import com.example.sar.internet.InternetListener
import com.example.sar.network.Resource
import com.example.testproject.R
import com.example.testproject.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(), InternetListener {

    private val viewModel: HomeViewModel by viewModels()

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setInternetListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textView.setOnClickListener {
            viewModel.login()
        }

        getFirstRequest()
    }

    private fun getFirstRequest() {

        viewModel.firstRequest.observe(viewLifecycleOwner, {

            when (it) {
                is Resource.Success -> {
                    it.value.strSessionID
                }

                is Resource.Loading -> {

                }

                is Resource.Failure -> {

                }
            }
        })
    }

    override fun internetConnected() {
        Log.e(HomeFragment::class.simpleName, "internetConnected:")
    }

    override fun internetNotConnected() {
        Log.e(HomeFragment::class.simpleName, "internetNotConnected:")
    }
}