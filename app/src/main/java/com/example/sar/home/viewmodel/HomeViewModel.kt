package com.example.sar.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.sar.network.response.GeneralResponse
import com.example.sar.base.BaseViewModel
import com.example.sar.home.repository.HomeRepository
import com.example.sar.login.model.LoginRequest
import com.example.sar.login.model.LoginResponse
import com.example.sar.network.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeRepository: HomeRepository) : BaseViewModel(){

    private val _firstRequest: MutableLiveData<Resource<LoginResponse>> =
        MutableLiveData()
    val firstRequest: LiveData<Resource<LoginResponse>> get() = _firstRequest

    fun login() {
        viewModelScope.launch {
            _firstRequest.value = Resource.Loading
            _firstRequest.value = homeRepository.login(LoginRequest())
        }
    }

}