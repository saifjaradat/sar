package com.example.sar.home.repository

import com.example.sar.base.BaseRepository
import com.example.sar.login.model.LoginRequest
import javax.inject.Inject

class HomeRepository @Inject constructor(): BaseRepository() {

    suspend fun login(loginRequest: LoginRequest) = safeApiCall {
        api.login(loginRequest)
    }
}