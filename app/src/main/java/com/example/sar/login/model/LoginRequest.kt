package com.example.sar.login.model


import com.example.sar.app.App
import com.example.sar.network.Header
import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("deviceModel")
    var deviceModel: DeviceModel = DeviceModel(),
    @SerializedName("header")
    var header: Header = Header()
) {
    data class DeviceModel(
        @SerializedName("appVersion")
        var appVersion: String = "2.0.9",
        @SerializedName("deviceId")
        var deviceId: String = App.deviceId,
        @SerializedName("deviceOS")
        var deviceOS: String = "Android SDK built for x86 7.0",
        @SerializedName("deviceToken")
        var deviceToken: String = "",
        @SerializedName("deviceTypeId")
        var deviceTypeId: Int = 1,
        @SerializedName("lookupVersion")
        var lookupVersion: String = "0.2"
    )
}