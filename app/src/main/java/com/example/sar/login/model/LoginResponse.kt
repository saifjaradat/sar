package com.example.sar.login.model


import com.example.sar.network.response.ResultResponse
import com.google.gson.annotations.SerializedName

class LoginResponse : ResultResponse() {

    @SerializedName("latestAppVersion")
    var latestAppVersion: Boolean = false

    @SerializedName("latestLookupVersion")
    var latestLookupVersion: Boolean = false

    @SerializedName("strSessionID")
    var strSessionID: String = ""
}
