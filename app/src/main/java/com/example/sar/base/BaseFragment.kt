package com.example.sar.base

import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.sar.internet.InternetListener
import com.example.sar.internet.NetworkChangeReceiver
import com.example.sar.internet.NetworkObservable
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

abstract class BaseFragment<B : ViewDataBinding> : Fragment(), Observer {

    protected lateinit var binding: B

    abstract fun getLayoutId(): Int

    private var broadcastReceiver: BroadcastReceiver? = null
    private var internetListener: InternetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initReceiver()
        container?.let { performDataBinding(inflater, it) }
        return binding.root
    }

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.executePendingBindings()
    }

    fun setInternetListener(internetListener: InternetListener) {
        this.internetListener = internetListener
    }

    private fun initReceiver() {
        broadcastReceiver = NetworkChangeReceiver()
    }

    private fun registerNetworkBroadcastReceiver() {
        context?.registerReceiver(
            broadcastReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    private fun unRegisterNetworkBroadcastReceiver() {
        context?.unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerNetworkBroadcastReceiver()
        NetworkChangeReceiver.getObservable()?.addObserver(this)
    }

    override fun onPause() {
        super.onPause()
        unRegisterNetworkBroadcastReceiver()
        NetworkChangeReceiver.getObservable()?.deleteObserver(this)
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is NetworkObservable) {
            try {
                if (NetworkChangeReceiver.getObservable()?.isConnected() == true) {
                    internetListener?.internetConnected()
                } else {
                    internetListener?.internetNotConnected()
                }

            } catch (exception: Exception) {

            }
        }
    }
}