package com.example.sar.base

import com.example.sar.network.ApiClient
import com.example.sar.network.SafeApiCall


abstract class BaseRepository : SafeApiCall {

    val api by lazy {
        ApiClient.create()
    }
}