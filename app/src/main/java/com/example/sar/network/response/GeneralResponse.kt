package com.example.sar.network.response

import com.google.gson.annotations.SerializedName

class GeneralResponse<T> {
    @SerializedName("resultResponse")
    var resultResponse: ResultResponse? = null
}