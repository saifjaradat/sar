package com.example.sar.network

import com.example.testproject.BuildConfig
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object ApiClient {

    @Singleton
    @Provides
    fun create(): ApiServices {

        val rxJava = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())!!
        val retrofit: Retrofit = Retrofit.Builder()
            .client(createOkHttp())
            .addCallAdapterFactory(rxJava)
            .addConverterFactory(GsonConverterFactory.create(createGson()))
            .baseUrl(createBaseUrl())
            .build()

        return retrofit.create(ApiServices::class.java)
    }

    private fun createGson() = GsonBuilder().setLenient().create()

    private fun createBaseUrl(): String = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun createOkHttp(): OkHttpClient {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)


        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor(Interceptor { chain: Interceptor.Chain ->
            val original = chain.request()

            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Request-Type", "Android")
                .addHeader("Content-Type", "application/json")


            val request: Request = requestBuilder.build()
            chain.proceed(request)
        })

        return httpClient.build()
    }
}