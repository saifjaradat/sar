package com.example.sar.network.response

import com.google.gson.annotations.SerializedName

open class ResultResponse() {
    @SerializedName("code")
    val code: String = ""

    @SerializedName("message")
    val message: String = ""
}
