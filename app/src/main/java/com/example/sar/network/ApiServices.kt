package com.example.sar.network

import com.example.sar.login.model.LoginRequest
import com.example.sar.login.model.LoginResponse
import com.example.sar.network.response.GeneralResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiServices {

    @POST("Profile/Login")
    suspend fun login(@Body loginRequest: LoginRequest): LoginResponse
}