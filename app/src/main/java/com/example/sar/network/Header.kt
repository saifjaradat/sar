package com.example.sar.network


import com.google.gson.annotations.SerializedName

data class Header(
    @SerializedName("channelId")
    var channelId: String = "004",
    @SerializedName("language")
    var language: String = "en-gb",
    @SerializedName("sessionID")
    var sessionID: String = "",
    @SerializedName("userCredential")
    var userCredential: String = "en",
    @SerializedName("userId")
    var userId: String = "23009",
    @SerializedName("userRole")
    var userRole: String = "1944"
)