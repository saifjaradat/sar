package com.example.sar.app

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.provider.Settings
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        var deviceId = ""
    }
}