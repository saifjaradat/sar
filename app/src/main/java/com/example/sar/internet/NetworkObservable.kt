package com.example.sar.internet

import java.util.*

class NetworkObservable private constructor() : Observable() {

    private var network = false

    fun connectionChanged(connection: Boolean) {
        network = connection
        setChanged()
        notifyObservers(connection)
    }

    fun isConnected(): Boolean = network

    companion object {

        private var instance: NetworkObservable? = null

        fun getInstance(): NetworkObservable? {
            if (instance == null) {
                instance = NetworkObservable()
            }
            return instance
        }
    }
}