package com.example.sar.internet


import com.example.testproject.R
import com.example.sar.app.App
import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String
        get() = com.example.sar.app.App.context.getString(R.string.no_internet_connection)
}