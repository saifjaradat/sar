package com.example.sar.internet

interface InternetListener {
    fun internetConnected()
    fun internetNotConnected()
}