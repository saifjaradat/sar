package com.example.sar.internet

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.sar.extenstions.isNetworkAvailable

class NetworkChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        try {
            getObservable()?.connectionChanged(isNetworkAvailable(context!!))
        } catch (exception: Exception) {
            Log.e(TAG, "onReceive: ", exception);
        }
    }

    companion object {
        const val TAG = "NetworkChangeReceiver"

        fun getObservable(): NetworkObservable? {
            return NetworkObservable.getInstance()
        }
    }
}